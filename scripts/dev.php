<?php

class dev
{
    public function deploy($isAll=false)
    {
        $srcPath = '/var/git_repo/develop'; //存放最新的git源代码目录
        $targetPath = '/path/to/dev'; //需要部署的目标目录
        $targetCachePath = array($targetPath . '/cache'); //目标目录的缓存目录, 每次部署会清空其内的文件
        
        //不会被部署到目标目录的文件
        $ignoreFiles = array(
            $srcPath.'.git',
            $srcPath.'README.md',
            $srcPath.'LICENSE',
            $srcPath.'.gitignore',
        );
        
        $tool = new Tool();
        $tool->ini($srcPath, $targetPath, $targetCachePath, $ignoreFiles);
        $tool->isShowResult = true;
        $tool->eof = PHP_EOL;
        $tool->logPath = ROOT.'msg.log';
        
        if (!empty($strCommitIds)) {
            $tool->setCommitIds($strCommitIds) //指定commit版本部署
                ->gitDiff() //获取变化
                ->deploy() //同步到目标目录
                ->clearCache() //清除指定的缓存目录
                ->over(); //备用, 可以做一些收尾工作
        } elseif (!$isAll) {
            $tool->gitPull() //拉取最新代码
                ->gitDiff() //获取变化
                ->deploy() //同步到目标目录
                ->clearCache() //清除指定的缓存目录
                ->over(); //备用, 可以做一些收尾工作
        } elseif ($isAll) {
            $tool->copyAll();
        } else {
            echo __FILE__. ' 参数错误';
        }
        
    }
}

